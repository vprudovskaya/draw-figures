import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-validation-error-message',
  templateUrl: './validation-error-message.component.html',
  styleUrls: ['./validation-error-message.component.less']
})
export class ValidationErrorMessageComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public errorString: string) {
  }

}
