import {Component, DoCheck} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {LineCaptionModel} from './shared/line-caption.model';
import {RectangleCaptionModel} from './shared/rectangle-caption.model';
import {TriangleCaptionModel} from './shared/triangle-caption.model';
import {CircleCaptionModel} from './shared/circle-caption.model';
import {EllipseCaptionModel} from './shared/ellipse-caption.model';
import {DataService} from './shared/services/data-service';
import {MatDialog} from '@angular/material/dialog';
import {ValidationErrorMessageComponent} from './validation-error-message/validation-error-message.component';

const hexFormatRegExp = new RegExp('^(#)((?:[A-Fa-f0-9]{3}){1,2})$', 'i');
const otherFormatsRegExp = new RegExp('^(rgb|hsl)(a?)[(]\\s*([\\d.]+\\s*%?)\\s*,\\s*([\\d.]+\\s*%?)\\s*,\\s*([\\d.]+\\s*%?)\\s*(?:,\\s*([\\d.]+)\\s*)?[)]$', 'i');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements DoCheck {
  public clearCanvasBtnDisabled = false;
  public figureCaptionControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private dataService: DataService,
              public dialog: MatDialog) {
  }

  ngDoCheck(): void {
    if (this.dataService.getAllFigures().length === 0) {
      this.clearCanvasBtnDisabled = true;
    } else {
      this.clearCanvasBtnDisabled = false;
    }
  }

  public _drawFigure(): void {
    let figureName: string = this.figureCaptionControl.value.split(' -p', 1)[0];
    if (figureName !== this.figureCaptionControl.value) {
      figureName = figureName.replace(/\s+/g, '');
      if (figureName === 'line') {
        this.createLine();
      } else if (figureName === 'rectangle') {
        this.createRectangle();
      } else if (figureName === 'triangle') {
        this.createTriangle();
      } else if (figureName === 'circle') {
        this.createCircle();
      } else if (figureName === 'ellipse') {
        this.createEllipse();
      } else {
        this.openErrorDialog('Figure name is incorrect');
      }
    } else {
      this.openErrorDialog('The format of the entered data is not correct');
    }
  }

  private createLine(): void {
    const pointsString = this.figureCaptionControl.value.split('-p')[1].split('-c')[0];
    const matches = pointsString.match(/\d+/g);
    const inputColor = this.figureCaptionControl.value.split('-c')[1];
    if (matches && (matches.length === 4) && (this.isColorValid(inputColor))) {
      const lineCaption: LineCaptionModel = {
        name: 'line',
        points: [[Number(matches[0]), Number(matches[1])], [Number(matches[2]), Number(matches[3])]],
        color: inputColor
      };
      this.dataService.addFigure(lineCaption);
    } else {
      this.openErrorDialog('The entered values are incorrect');
    }
  }

  private createRectangle(): void {
    const pointsString = this.figureCaptionControl.value.split('-p')[1].split('-c')[0];
    const matches = pointsString.match(/\d+/g);
    const colorsString: string = this.figureCaptionControl.value.split('-c')[1];
    const inputColor: string = colorsString.split('-b')[0];
    const inputBackgroundColor: string = colorsString.split('-b')[1];
    if (matches && (matches.length === 4) && this.isColorValid(inputColor) && this.isColorValid(inputBackgroundColor)) {
      const rectangleCaption: RectangleCaptionModel = {
        name: 'rectangle',
        points: [[Number(matches[0]), Number(matches[1])], [Number(matches[2]), Number(matches[3])]],
        color: inputColor,
        backgroundColor: inputBackgroundColor
      };
      this.dataService.addFigure(rectangleCaption);
    } else {
      this.openErrorDialog('The entered values are incorrect');
    }
  }

  private createTriangle(): void {
    const pointsString = this.figureCaptionControl.value.split('-p')[1].split('-c')[0];
    const matches = pointsString.match(/\d+/g);
    const colorsString: string = this.figureCaptionControl.value.split('-c')[1];
    const inputColor: string = colorsString.split('-b')[0];
    const inputBackgroundColor: string = colorsString.split('-b')[1];
    if (matches && (matches.length === 6) && this.isColorValid(inputColor) && this.isColorValid(inputBackgroundColor)) {
      const triangleCaption: TriangleCaptionModel = {
        name: 'triangle',
        points: [[Number(matches[0]), Number(matches[1])],
          [Number(matches[2]), Number(matches[3])],
          [Number(matches[4]), Number(matches[5])]],
        color: inputColor,
        backgroundColor: inputBackgroundColor
      };
      this.dataService.addFigure(triangleCaption);
    } else {
      this.openErrorDialog('The entered values are incorrect');
    }
  }

  private createCircle(): void {
    const pointsString = this.figureCaptionControl.value.split('-p')[1].split('-r')[0];
    const matches = pointsString.match(/\d+/g);
    const inputRadius: number = Number((this.figureCaptionControl.value.split('-r')[1]).split('-c')[0]);
    const colorsString: string = this.figureCaptionControl.value.split('-c')[1];
    const inputColor: string = colorsString.split('-b')[0];
    const inputBackgroundColor: string = colorsString.split('-b')[1];
    if (matches
      && (inputRadius > 0)
      && (matches.length === 2)
      && this.isColorValid(inputColor)
      && this.isColorValid(inputBackgroundColor)) {
      const circleCaption: CircleCaptionModel = {
        name: 'circle',
        points: [Number(matches[0]), Number(matches[1])],
        radius: inputRadius,
        color: inputColor,
        backgroundColor: inputBackgroundColor
      };
      this.dataService.addFigure(circleCaption);
    } else {
      this.openErrorDialog('The entered values are incorrect');
    }
  }

  private createEllipse(): void {
    const pointsString = this.figureCaptionControl.value.split('-p')[1].split('-r1')[0];
    const matches = pointsString.match(/\d+/g);
    const firstInputRadius = Number(this.figureCaptionControl.value.split('-r1')[1].split('-r2')[0]);
    const secondInputRadius = Number(this.figureCaptionControl.value.split('-r2')[1].split('-c')[0]);
    const colorsString: string = this.figureCaptionControl.value.split('-c')[1];
    const inputColor: string = colorsString.split('-b')[0];
    const inputBackgroundColor: string = colorsString.split('-b')[1];
    if (matches
      && (firstInputRadius > 0)
      && (secondInputRadius > 0)
      && (matches.length === 2)
      && this.isColorValid(inputColor)
      && this.isColorValid(inputBackgroundColor)) {
      const ellipseCaption: EllipseCaptionModel = {
        name: 'ellipse',
        points: [Number(matches[0]), Number(matches[1])],
        xRadius: firstInputRadius,
        yRadius: secondInputRadius,
        color: inputColor,
        backgroundColor: inputBackgroundColor
      };
      this.dataService.addFigure(ellipseCaption);
    } else {
      this.openErrorDialog('The entered values are incorrect');
    }
  }

  private isColorValid(str: string): boolean {
    str = str.replace(/\s+/g, '');
    if (str.match(hexFormatRegExp) || str.match(otherFormatsRegExp)) {
      return true;
    } else {
      return false;
    }
  }

  public _clearInputField(): void {
    this.figureCaptionControl.reset();
  }

  public _clearCanvas(): void {
    this.dataService.clearAll();
  }

  private openErrorDialog(errorString: string): void {
    this.dialog.open(ValidationErrorMessageComponent, {
      width: '300px',
      height: '200px',
      data: errorString
    });
  }
}
