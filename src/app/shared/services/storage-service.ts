export class StorageService {

  public saveItems(array: any): void {
    sessionStorage.setItem('figures', JSON.stringify(array));
  }

  public getAll(): any[] {
    if (sessionStorage.getItem('figures')) {
      return JSON.parse(sessionStorage.getItem('figures') || '');
    } else {
      return [];
    }
  }

  public deleteAll(): void {
    sessionStorage.clear();
  }
}
