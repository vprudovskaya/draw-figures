import {StorageService} from './storage-service';
import {Injectable} from '@angular/core';

@Injectable()
export class DataService {
  private figures: any[] = [];

  constructor(private storageService: StorageService) {
  }

  public addFigure(figure: any): void {
    this.figures.push(figure);
    this.storageService.saveItems(this.figures);
  }

  public getAllFigures(): any[] {
    if (this.figures.length) {
      return this.figures;
    } else {
      return this.storageService.getAll();
    }
  }

  public clearAll(): void {
    this.figures = [];
    this.storageService.deleteAll();
  }
}
