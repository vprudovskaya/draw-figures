export interface TriangleCaptionModel {
  name: string;
  points: [number[], number[], number[]];
  color: string;
  backgroundColor: string;
}
