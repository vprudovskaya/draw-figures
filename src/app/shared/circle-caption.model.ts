export interface CircleCaptionModel {
  name: string;
  points: number[];
  radius: number;
  color: string;
  backgroundColor: string;
}
