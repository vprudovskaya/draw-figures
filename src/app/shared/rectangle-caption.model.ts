export interface RectangleCaptionModel {
  name: string;
  points: [number[], number[]];
  color: string;
  backgroundColor: string;
}
