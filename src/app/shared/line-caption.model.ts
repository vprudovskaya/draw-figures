export interface LineCaptionModel {
  name: string;
  points: [number[], number[]];
  color: string;
}
