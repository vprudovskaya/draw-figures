export interface EllipseCaptionModel {
  name: string;
  points: number[];
  xRadius: number;
  yRadius: number;
  color: string;
  backgroundColor: string;
}
