import {
  Component,
  ViewChild,
  ElementRef,
  DoCheck,
  AfterViewChecked
} from '@angular/core';
import {DataService} from '../shared/services/data-service';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.less']
})
export class CanvasComponent implements DoCheck, AfterViewChecked {
  private figureCaptions: any[] = [];
  @ViewChild('canvasElement') canvas!: ElementRef<HTMLCanvasElement>;

  private context!: CanvasRenderingContext2D | null;

  constructor(private dataService: DataService) {
  }

  ngDoCheck(): void {
    this.figureCaptions = this.dataService.getAllFigures();
  }

  ngAfterViewChecked(): void {
    this.context = (this.canvas.nativeElement as HTMLCanvasElement).getContext('2d');
    this.context?.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    for (const figure of this.figureCaptions) {
      if (figure.name === 'line') {
        const startX: number = figure.points[0][0];
        const startY: number = figure.points[0][1];
        const endX: number = figure.points[1][0];
        const endY: number = figure.points[1][1];
        const color: string = figure.color;
        this.drawLine(startX, startY, endX, endY, color);
      } else if (figure.name === 'rectangle') {
        const width: number = figure.points[1][0] - figure.points[0][0];
        const height: number = figure.points[1][1] = figure.points[0][1];
        const leftUpperAngleX: number = figure.points[0][0];
        const leftUpperAngleY: number = figure.points[1][1];
        const color: string = figure.color;
        const backgroundColor: string = figure.backgroundColor;
        this.drawRectangle(leftUpperAngleX, leftUpperAngleY, width, height, color, backgroundColor);
      } else if (figure.name === 'triangle') {
        const firstX: number = figure.points[0][0];
        const firstY: number = figure.points[0][1];
        const secondX: number = figure.points[1][0];
        const secondY: number = figure.points[1][1];
        const thirdX: number = figure.points[2][0];
        const thirdY: number = figure.points[2][1];
        const color: string = figure.color;
        const backgroundColor: string = figure.backgroundColor;
        this.drawTriangle(firstX, firstY, secondX, secondY, thirdX, thirdY, color, backgroundColor);
      } else if (figure.name === 'circle') {
        const radius: number = figure.radius;
        const startX: number = figure.points[0];
        const startY: number = figure.points[1];
        const color: string = figure.color;
        const backgroundColor: string = figure.backgroundColor;
        this.drawCircle(startX, startY, radius, color, backgroundColor);
      } else if (figure.name === 'ellipse') {
        const x: number = figure.points[0];
        const y: number = figure.points[1];
        const xRadius: number = figure.xRadius;
        const yRadius: number = figure.yRadius;
        this.drawEllipse(x, y, xRadius, yRadius, figure.color, figure.backgroundColor);
      }
    }
  }

  private drawLine(startX: number, startY: number, endX: number, endY: number, color: string): void {
    this.context?.beginPath();
    this.context?.moveTo(startX, startY);
    this.context?.lineTo(endX, endY);
    this.context?.closePath();
    this.context!.strokeStyle = color;
    this.context?.stroke();
  }

  private drawRectangle(leftUpperAngleX: number,
                        leftUpperAngleY: number,
                        width: number,
                        height: number,
                        color: string,
                        backgroundColor: string): void {
    this.context?.beginPath();
    this.context?.rect(leftUpperAngleX, leftUpperAngleY, width, height);
    this.context?.closePath();
    this.context!.strokeStyle = color;
    this.context!.fillStyle = backgroundColor;
    this.context?.fill();
    this.context?.stroke();
  }

  private drawTriangle(firstX: number,
                       firstY: number,
                       secondX: number,
                       secondY: number,
                       thirdX: number,
                       thirdY: number,
                       color: string,
                       backgroundColor: string): void {
    this.context?.beginPath();
    this.context?.moveTo(firstX, firstY);
    this.context?.lineTo(secondX, secondY);
    this.context?.lineTo(thirdX, thirdY);
    this.context?.closePath();
    this.context!.strokeStyle = color;
    this.context!.fillStyle = backgroundColor;
    this.context?.fill();
    this.context?.stroke();
  }

  private drawCircle(startX: number, startY: number, radius: number, color: string, backgroundColor: string): void {
    this.context?.beginPath();
    this.context?.arc(startX, startY, radius, 0, (2 * Math.PI), false);
    this.context?.closePath();
    this.context!.strokeStyle = color;
    this.context!.fillStyle = backgroundColor;
    this.context?.fill();
    this.context?.stroke();
  }

  private drawEllipse(x: number, y: number, xRadius: number, yRadius: number, color: string, backgroundColor: string): void {
    this.context?.beginPath();
    this.context?.ellipse(x, y, xRadius, yRadius, 0, 0, (2 * Math.PI), false);
    this.context?.closePath();
    this.context!.strokeStyle = color;
    this.context!.fillStyle = backgroundColor;
    this.context?.fill();
    this.context?.stroke();
  }
}
