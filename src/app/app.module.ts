import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {CanvasComponent} from './canvas/canvas.component';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {DataService} from './shared/services/data-service';
import {MatDialogModule} from '@angular/material/dialog';
import { ValidationErrorMessageComponent } from './validation-error-message/validation-error-message.component';
import {StorageService} from './shared/services/storage-service';

@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    ValidationErrorMessageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule
  ],
  providers: [
    StorageService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
