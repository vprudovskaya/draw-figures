# DrawFigures



Installation
  ------------
  
To install the dependencies run **npm install** 
command in the project directory.


Application launch
  ------------
  
To start the application run **npm run start** 
command and then open your browser on localhost:4200.

Usage
  ------------
This application was developed for drawing figures 
from their description. 
There are some figures that can be drawn: 
line, rectangle, triangle, circle, ellipse.

You can draw several shapes together 
by entering their descriptions one by one.
You can clear the board anytime.


*Input pattern:*
figureName -p [x, y] ... -c color -b backgroundColor

Colors can be entered in various formats: 
rgb, rgba, hex, hsl, hsla.

*For example:*
ellipse -p [75, 75] -r1 50 -r2 25 -c #8B0000 -b rgba(41, 158, 158, .2)

![Result for this example](./src/assets/screenshots/ellipse.jpg)
